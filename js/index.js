$(function () {
    $('[data-bs-toggle="tooltip"]').tooltip();
    $('[data-bs-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 5000
    });
    $('#exampleModal').on('show.bs.modal', function (e) {
        console.log('El modal se esta mostrando');
        $('#contactoSanPablo').removeClass('btn-outline-success');
        $('#contactoSanPablo').addClass('btn-primary');
        $('#contactoSanPablo').prop('disabled', true);
    });
    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log('El modal se mostró');
    });
    $('#exampleModal').on('hide.bs.modal', function (e) {
        console.log('El modal se oculta');
    });
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        console.log('El modal se ocultó');
        $('#contactoSanPablo').prop('disabled', false);
    });
});